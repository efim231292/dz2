﻿import java.util.Arrays;

    public class Tu {
        public static void Main(String[] args) {

            float mass[] = {20, 50, 30, 10, 40};

            System.out.println("\n Исходный массив ");
            System.out.println(Arrays.toString(mass));


            for (int i = 0; i < mass.length; i++) mass[i] = (float) (mass[i] * 1.1);


            for (int start = 0; start < mass.length - 1; start++) {
                for (int index = 0; index < mass.length - 1 - start; index++) {
                    if (mass[index] < mass[index + 1]){
                        swap(mass, index);
                    }
                }
            }
            System.out.println("\nПолученный массив методом пузырька и увеличенный на 10% ");
            System.out.println(Arrays.toString(mass));
        }

        private static void swap(float array[], int i) {
            float tmp = array[i];
            array[i] = array [i + 1];
            array [i + 1]= tmp;
        }

    }

